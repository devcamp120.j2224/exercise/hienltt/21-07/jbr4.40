package com.devcamp.s50.customerinvoice_api.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.customerinvoice_api.model.Customer;
import com.devcamp.s50.customerinvoice_api.model.Invoice;

@RestController
public class CustomerInvoiceControl {
    @CrossOrigin
    @GetMapping("/invoices")
    public ArrayList<Invoice> getListInvoice(){

        Customer customer1 = new Customer(1, "Nguyen Van A", 10);
        Customer customer2 = new Customer(2, "Nguyen Van C", 15);
        Customer customer3 = new Customer(3, "Nguyen Van E", 12);
        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3);

        Invoice invoice1 = new Invoice(001, customer1, 15000000);
        Invoice invoice2 = new Invoice(002, customer2, 17000000);
        Invoice invoice3 = new Invoice(003, customer3, 10000000);
        System.out.println(invoice1);
        System.out.println(invoice2);
        System.out.println(invoice3);

        ArrayList <Invoice> listInvoice = new ArrayList<>();
        listInvoice.add(invoice1);
        listInvoice.add(invoice2);
        listInvoice.add(invoice3);
        return listInvoice;
    }
}
